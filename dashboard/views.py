from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from geopy.geocoders import Nominatim
from datetime import datetime, timedelta
from plotly.offline import plot

import plotly.graph_objs as go
import random, folium

class create_dict(dict):
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        self[key] = val

# Create your views here.
@login_required(login_url='/Gateway/login_auth/')
def Home(request):
    return render(request,'index.html')

def login_auth(request):
    return render(request, 'login.html')

@csrf_exempt
def Sensors(request):
    username = request.POST.get('gfr_username')
    password = request.POST.get('gfr_password')
    request.session['gfr_user'] = username 
    request.session['gfr_pass'] = password

    if username == "admin" and password == "pwd@admin"  or 'gfr_user' in request.session:
        return render(request,'index.html')
    else :
        messages = "Invalid Credential"
        return render(request, 'login.html',{'error_message':messages})

def Temperature(request):

    coordinates = [[12.76472599132344, 77.81981436732111, "Gateway1"], [12.421759526925738, 76.56806237385139, "Gateway2"], [8.274174951313928, 77.88915007936491, "Gateway3"], [9.13836395223995, 76.85613761207748, "Gateway4"], [14.215246189073836, 74.9206675225612, "Gateway5"], [17.38812770394452, 78.46727085147914, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'temperature.html', context=mydict)

def WaterLevel(request):
    coordinates = [[13.131840240651233, 80.30325829784756, "Gateway1"], [12.761235675916195, 79.90822281984353, "Gateway2"], [11.786373165344202, 77.8008817602581, "Gateway3"], [11.442830422796906, 77.07488701356273, "Gateway4"], [11.947375422818357, 76.29736681835267, "Gateway5"], [8.781058468234708, 78.16118663960386, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'waterlevel.html',context=mydict)

def AirQuality(request):
    coordinates = [[28.644110320386297, 77.21253099594207, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [23.301531930738104, 85.01478868883494, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'airquality.html',context=mydict)

def Gas(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'gas.html',context=mydict)

def Biometric(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'biometric.html',context=mydict)

def Humidity(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'humidity.html',context=mydict)

def IMU(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'imu.html',context=mydict)

def Vibration(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'vibration.html',context=mydict)

def Measurement(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'measurement.html',context=mydict)

def Proximity(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'proximity.html',context=mydict)

def pH(request):

    coordinates = [[12.76472599132344, 77.81981436732111, "Gateway1"], [12.421759526925738, 76.56806237385139, "Gateway2"], [8.274174951313928, 77.88915007936491, "Gateway3"], [9.13836395223995, 76.85613761207748, "Gateway4"], [14.215246189073836, 74.9206675225612, "Gateway5"], [17.38812770394452, 78.46727085147914, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'ph.html',context=mydict)

def Fuel(request):

    coordinates = [[14.621181853608736, 76.0454835856738, "Gateway1"], [19.529464087650094, 75.39575224914222, "Gateway2"], [18.827610547665717, 82.35763107410773, "Gateway3"], [13.069696568704355, 80.19484755422246, "Gateway4"], [9.433663642885243, 77.52702333859675, "Gateway5"], [12.344177179915782, 76.58704618684612, "Gateway6"]]
    
    m = folium.Map(location=[20.5937, 78.9629], width="%100", height="100%",zoom_start=4, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    marker_group = folium.FeatureGroup(name='Markers')

    geolocator = Nominatim(user_agent="my_geocoder")
    
    for row in coordinates:
        try:
            location = geolocator.reverse((row[0], row[1]), exactly_one=True)

        except GeocoderServiceError as e:
            continue

        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Tracker ID :"+str(row[2])
        marker = folium.Marker(([row[0],row[1]]),tooltip=tooltip)
        marker.add_to(marker_group)

    marker_group.add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict['map'] = ms

    return render(request,'fuel.html',context=mydict)

def Logout(request):
    return HttpResponseRedirect('/Gateway/')

def Dispenser(request):
    today = datetime.now().date()

    d_t = []
    u_h = []
    for i in range(7):
        date = today - timedelta(days=i)
        usage = random.randint(50, 250)
        d_t.append(date.strftime('%Y-%m-%d'))
        u_h.append(usage)
    
    data = [
        go.Bar(
            x=d_t,
            y=u_h,
            marker=dict(color='rgb(26, 118, 255)')
        )
    ]
    layout = go.Layout(
        title='Daily Usage History',
        xaxis=dict(title='Date'),
        yaxis=dict(title='Usage'),
        bargap=0.2,
    )
    fig = go.Figure(data=data, layout=layout)
    plot_div = plot(fig, output_type='div')

    return render(request,"dispenser.html",context={'plot_div':plot_div})

def DispenserHist(request):
    today = datetime.now().date()

    d_t = []
    u_h = []
    for i in range(7):
        date = today - timedelta(days=i)
        usage = random.randint(50, 250)
        d_t.append(date.strftime('%Y-%m-%d'))
        u_h.append(usage)
    
    data = [
        go.Bar(
            x=d_t,
            y=u_h,
            marker=dict(color='rgb(26, 118, 255)')
        )
    ]
    layout = go.Layout(
        title='Daily Usage History',
        xaxis=dict(title='Date'),
        yaxis=dict(title='Usage'),
        bargap=0.2,
    )
    fig = go.Figure(data=data, layout=layout)
    plot_div = plot(fig, output_type='div')

    return HttpResponse("ok")