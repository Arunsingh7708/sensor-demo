from django.conf.urls import url
from .views  import *

urlpatterns = [
    url(r'^$',Home,name='home'),
    url(r'^Sensors/',Sensors,name='sensors'),
    url(r'^login_auth/',login_auth,name='login_auth'),
    url(r'^Temperature/',Temperature,name='temperature'),
    url(r'^WaterLevel/',WaterLevel,name='waterlevel'),
    url(r'^AirQuality/',AirQuality,name='airquality'),
    url(r'^Gas/',Gas,name='gas'),
    url(r'^Biometric/',Biometric,name='biometric'),
    url(r'^Humidity/',Humidity,name='humidity'),
    url(r'^IMU/',IMU,name='imu'),
    url(r'^Vibration/',Vibration,name='vibration'),
    url(r'^Biometric/',Biometric,name='biometric'),
    url(r'^Measurement/',Measurement,name='measurement'),
    url(r'^Proximity/',Proximity,name='proximity'),
    url(r'^pH/',pH,name='pH'),
    url(r'^Fuel/',Fuel,name='fuel'),
    url(r'^dispenser/',Dispenser,name='dispenser'),
    url(r'^dispenserhistory/',DispenserHist,name='dispenserhist'),
    url(r'^logout/',Logout,name='logout'),
]